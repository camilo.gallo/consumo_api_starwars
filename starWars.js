const app = document.getElementById('root');
const container = document.createElement('div');
container.setAttribute('class', 'container');
const containerOne = document.createElement('div');
containerOne.setAttribute('class', 'containerOne');

const logo = document.createElement('img')
logo.src = 'starWars.png';
const welcome = document.createElement('h1');
welcome.setAttribute('class', 'welcome');
welcome.textContent = "WELCOME TO MY PAGE";



app.appendChild(containerOne);
app.appendChild(container);
containerOne.appendChild(logo);
containerOne.appendChild(welcome);


let req = new XMLHttpRequest();
req.open('GET', 'https://swapi.co/api/films/?format=json', true);
req.onload = function() {
    let data = JSON.parse(this.response);
    if (req.status >= 200 && req.status < 400) {
        let myObjetc = data.results;
        //console.log(data.results);
        myObjetc.forEach(element => {
            // console.log(element.title);
            // console.log(`Fecha de lanzamiento : ${element.release_date}`);
            // console.log(`Episodio : ${element.episode_id}`);
            // console.log(`Descripcion : ${element.opening_crawl}`);
            // console.log(`Productor : ${element.producer}`);
            // console.log(`Director : ${element.director}`);
            const card = document.createElement('div');
            card.setAttribute('class', 'card');

           
            const h1 = document.createElement('h1');
            h1.setAttribute('class', 'titleMovies');
            h1.textContent = element.title;

            const span = document.createElement('span');
            span.textContent = `Fecha de lanzamiento : ${element.release_date}`;

            const h4 = document.createElement('h4');
            h4.textContent = `Episodio : ${element.episode_id}`;

           
            const p = document.createElement('p');
            element.opening_crawl = element.opening_crawl.substring(0, 300); 
            p.textContent = `${element.opening_crawl}...`; 

            const h6 = document.createElement('h6');
            h6.textContent = `Productor : ${element.producer}`;

            const h7 = document.createElement('span');
	    h7.setAttribute('class', 'h7');
            h7.textContent = `Director : ${element.director}`;

           
            container.appendChild(card);

           
            card.appendChild(h1);
            card.appendChild(span);
            card.appendChild(h4);
            card.appendChild(p);
            card.appendChild(h6);
            card.appendChild(h7)

        });
    } else {
        console.log('error');

    }
}

req.send();
